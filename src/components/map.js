import React from 'react';
import {connect} from 'react-redux';
import {actions} from "../redux/action";
import GoogleMapReact from 'google-map-react';
import Driver from './driver'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'

class Map extends React.PureComponent {
    static defaultProps = {
        zoom: 15
    };

    componentDidMount() {
       this.getData();
    }

    getData = ()=>{
        let {numberOfDriver, dispatch, currentLocation} = this.props
        dispatch(actions.getData(numberOfDriver, currentLocation))
    };

    renderDrivers = ()=>{
      const {map} = this.props;
      if(!map.data.drivers){
          return null;
      }
      return map.data.drivers.map((driver)=>{
          return <Driver key={driver.location.longitude} lat={driver.location.longitude}
                          lng={driver.location.latitude}
                          />
      })
    };

    handleNumberOfDriverChange = value => {
        this.props.dispatch(actions.changeNumberOfDriver(value))
    };

    renderSlider = ()=>{
        const {numberOfDriver} = this.props;
        return(<div>
            <div>Drivers to show: {numberOfDriver}</div>
            <Slider
                min={10}
                max={30}
                step={10}
                value={numberOfDriver}
                onChange={this.handleNumberOfDriverChange}
            />
        </div>)
    };

    render() {
        let {isLoading, currentLocation, zoom} = this.props;
        if (!isLoading) {
            return <div>is loading...</div>
        }
        return(<div className="map-container">
            <div className="side-bar">
                {this.renderSlider()}
            </div>
            <div className="map-wrapper">
                <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyAqAEX7qxapmVUvKoYjQ7tc7VOW33HYuVw' }}
                    defaultCenter={currentLocation}
                    defaultZoom={zoom}>
                    {this.renderDrivers()}
                </GoogleMapReact>
            </div>
        </div>)
    }


}

const mapDispatchToProps = dispatch => {
    return {
        dispatch: dispatch
    };
};

const mapStateToProps = state => {
    return {
        map: state.map,
        numberOfDriver: state.numberOfDriver,
        currentLocation: state.currentLocation
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Map);

