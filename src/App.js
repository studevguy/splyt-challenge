import React from 'react';
import { Provider } from 'react-redux';
import {initStore} from "./redux/store";
import Map from "./components/map"

import css from './assets/styles/main.scss'

const store = initStore();

export default class App extends React.PureComponent {
    render(){
        return <Provider store={store}>
                    <Map/>
                </Provider>
    }
}
