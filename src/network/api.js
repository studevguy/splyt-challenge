import fetch from 'isomorphic-unfetch'
const CALL_API = 'call_api';

export class Api {
    static rootUrl = 'http://demo8816636.mockable.io/';
    static methods = {
        get: 'GET',
        post: 'POST',
        delete: 'DELETE',
        put: 'PUT',
        patch: 'PATCH'
    };

    static  formatRequestData(requestData) {
        //const userId = getSessionData();
        const userId = "Admin";
        return {
            meta: {
                userId
            },
            data: {
                ...requestData,
                userId
            }
        }
    }

    static async callApi(endpoint, method, requestData) {
        let url = (endpoint.indexOf('http://') >= 0) ? endpoint : this.rootUrl + endpoint;
        const request = {
            method: method
        };


        if (method !== this.methods.get) {
            requestData =  Api.formatRequestData(requestData);
            request.body = JSON.stringify(requestData);
            request.headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }
        console.log('----------Request Url :-------' + url)

        return fetch(url, request).then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json)
                }
                return json;
            })
        ).catch((err) => {
            console.log(JSON.stringify(err))
            return Promise.reject({})
        })

    }

    static request(url, actionTypes, data, requestType) {
        return {
            [CALL_API]: {
                types: actionTypes,
                endpoint: url,
                requestType,
                data: data
            }
        }
    }

    static get = (url, actionTypes, data) => {
        console.log(url)
        return Api.request(url, actionTypes, data, Api.methods.get);
    };

    static post = (url, actionTypes, data) => {
        return Api.request(url, actionTypes, data, Api.methods.post);
    };

    static delete = (url, actionTypes, data) => {
        return Api.request(url, actionTypes, data, Api.methods.delete);
    };

    static patch = (url, actionTypes, data) => {
        return Api.request(url, actionTypes, data, Api.methods.patch);
    };

    static put = (url, actionTypes, data) => {
        return Api.request(url, actionTypes, data, Api.methods.put);
    }
}

export default store => next => action => {
    const actionData = action[CALL_API];
    if (!actionData) {
        return next(action);
    }

    const {data = {}, types, requestType, endpoint} = actionData;

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.');
    }

    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.');
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('Expected action types to be strings.')
    }

    const getAction = data => {
        const finalAction = Object.assign({}, action, data);
        delete finalAction[CALL_API];
        return finalAction;
    };

    const [request, successType, failureType] = types;

    next(getAction({type: request,requestData: data || {}, ignore: data.ignore}));
    return Api.callApi(endpoint, requestType, data).then(
        (response) => {
            console.log(JSON.stringify(response))
            //console.log(JSON.stringify(response))
            if (!response || response.error) {
                return handleError(response || {});
            }
            return next(getAction({
                response,
                type: successType,
                ignore: data.ignore,
                requestData: data || {}
            }));
        },
        (error) => {
            handleError(error);
        });

    function handleError(error) {
        return next(getAction({
            type: failureType,
            ignore: data.ignore,
            error: true,
            response: {
                message: error.errorMessage || 'Something bad happened'
            }

        }));
    }
}
