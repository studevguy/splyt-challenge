import {reduxConstant} from "../constant/index";

export const currentLocation = (state = {lat: 1.3030487878, lng: 103.825808363}, action) => {
    switch (action.type) {
        case(reduxConstant.CHANGE_NUMBER_OF_DRIVER): {
            return {lat: action.lat, lng: action.lng}
        }

        default : {
            return state;
        }
    }

};
