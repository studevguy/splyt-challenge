import {reduxConstant} from "../constant/index";

export const numberOfDriver = (state = 10, action) => {
    switch (action.type) {
        case(reduxConstant.CHANGE_NUMBER_OF_DRIVER): {
            return action.numberOfDriver
        }

        default : {
            return state;
        }
    }

};
