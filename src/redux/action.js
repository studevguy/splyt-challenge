import {reduxConstant} from '../constant';
import {Api} from "../network/api";


export const actions = {
    getData: (drivers, currentLocation) => (dispatch) => {
        let endpoint = `api/drivers?latitude=${currentLocation.lat}&longitude=${currentLocation.lng}&count=${drivers}`;
        return dispatch(Api.get(endpoint, [reduxConstant.GET_DATA, reduxConstant.GET_DATA_SUCCESS, reduxConstant.GET_DATA_FAIL]));
    },

    changeNumberOfDriver: (number) => (dispatch) => {
        dispatch({type: reduxConstant.CHANGE_NUMBER_OF_DRIVER, numberOfDriver: number})
    }
};
