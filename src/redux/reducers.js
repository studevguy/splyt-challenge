import { combineReducers } from 'redux';
import {map} from "./mapReducer";
import {numberOfDriver} from "./numberOfDriverReducer";
import {currentLocation} from "./currentLocationReducer";


export default combineReducers({
    map,
    numberOfDriver,
    currentLocation
});
