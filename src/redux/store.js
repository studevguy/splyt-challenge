import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import thunkMiddleware from "redux-thunk";
import reducer from "./reducers";
import api from '../network/api'


export const initStore = (state) => {
    return createStore(
        reducer,
        state,
        composeWithDevTools(
            applyMiddleware(thunkMiddleware, api)
        )

    );
};

export  default  initStore
