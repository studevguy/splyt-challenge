import {reduxConstant} from "../constant/index";

export const map = (state = {
    data: [],
    isLoading: false,
}, action) => {
    switch (action.type) {
        case(reduxConstant.GET_DATA): {
            return {
                ...state,
                isLoading: true

            }
        }

        case(reduxConstant.GET_DATA_SUCCESS): {
            const data = action.response.data;
            return {
                ...state,
                data,
                isLoading: false
            }
        }

        case(reduxConstant.GET_DATA_FAIL): {
            return {
                ...state,
                isLoading: false
            }
        }

        default : {
            return state;
        }
    }
};
